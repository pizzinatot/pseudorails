class PseudosController < ApplicationController
  before_action :set_pseudo, only: [:show, :edit, :update, :destroy]

  # GET /pseudos
  # GET /pseudos.json
  def index
    @pseudos = Pseudo.all
  end

  # GET /pseudos/1
  # GET /pseudos/1.json
  def show
  end

  # GET /pseudos/new
  def new
    @pseudo = Pseudo.new
  end

  # GET /pseudos/1/edit
  def edit
  end

  def generate_pseudo
    o = [('A'..'Z')].map(&:to_a).flatten
    string = (0...3).map { o[rand(o.length)] }.join

    @pseudo = Pseudo.new(:pseudo => string)

      if @pseudo.save
        respond_to do |format|
        format.html { redirect_to @pseudo, notice: 'OK, your pseudo was successfully created.' }
        format.json { render :show, status: :created, location: @pseudo }
        end
      else
        generate_pseudo
        #format.html { render :new }
        #format.json { render json: @pseudo.errors, status: :unprocessable_entity }
      end
  end

  # POST /pseudos
  # POST /pseudos.json

  def create

    @pseudo = Pseudo.new(pseudo_params)
      if @pseudo.save
        respond_to do |format|
        format.html { redirect_to @pseudo, notice: 'OK, your pseudo was successfully created.' }
        format.json { render :show, status: :created, location: @pseudo }
        end
      elsif @pseudo.pseudo.length < 3
        respond_to do |format|
        format.html { render :new }
        format.json { render json: @pseudo.errors, status: :unprocessable_entity }
      end
      else
        generate_pseudo
      end
  end

  # PATCH/PUT /pseudos/1
  # PATCH/PUT /pseudos/1.json
  def update
    respond_to do |format|
      if @pseudo.update(pseudo_params)
        format.html { redirect_to @pseudo, notice: 'OK, your pseudo was successfully updated.' }
        format.json { render :show, status: :ok, location: @pseudo }
      else
        format.html { render :edit }
        format.json { render json: @pseudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pseudos/1
  # DELETE /pseudos/1.json
  def destroy
    @pseudo.destroy
    respond_to do |format|
      format.html { redirect_to pseudos_url, notice: 'OK, your pseudo was successfully destroyed.' }
      format.json { head :no_content, status: 'SUCCESS'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pseudo
      @pseudo = Pseudo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def pseudo_params
      params.require(:pseudo).permit(:pseudo)
    end
end
