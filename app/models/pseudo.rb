class Pseudo < ApplicationRecord


  validates :pseudo, presence: true
  validates :pseudo, format: { with: /\A[A-Z]+\Z/,
    message: "only allows upper letters" }
  validates :pseudo, length: {is: 3}
  validates :pseudo, uniqueness: true

  #after_validation :uniqueness_pseudo

  before_save :uppercase_pseudo

  def uppercase_pseudo
      pseudo.upcase!
  end

end
