class CreatePseudos < ActiveRecord::Migration[5.2]
  def change
    create_table :pseudos do |t|
      t.string :pseudo
      t.timestamps
    end
  end
end
