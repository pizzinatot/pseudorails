require "rails_helper"

RSpec.describe PseudosController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/pseudos").to route_to("pseudos#index")
    end

    it "routes to #new" do
      expect(get: "/pseudos/new").to route_to("pseudos#new")
    end

    it "routes to #show" do
      expect(get: "/pseudos/1").to route_to("pseudos#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/pseudos/1/edit").to route_to("pseudos#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/pseudos").to route_to("pseudos#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/pseudos/1").to route_to("pseudos#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/pseudos/1").to route_to("pseudos#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/pseudos/1").to route_to("pseudos#destroy", id: "1")
    end
  end
end
