require 'rails_helper'

RSpec.describe "pseudos/index", type: :view do
  before(:each) do
    assign(:pseudos, [
      Pseudo.create!(
        pseudo: "Pseudo"
      ),
      Pseudo.create!(
        pseudo: "Pseudo"
      )
    ])
  end

  it "renders a list of pseudos" do
    render
    assert_select "tr>td", text: "Pseudo".to_s, count: 2
  end
end
