require 'rails_helper'

RSpec.describe "pseudos/new", type: :view do
  before(:each) do
    assign(:pseudo, Pseudo.new(
      pseudo: "MyString"
    ))
  end

  it "renders new pseudo form" do
    render

    assert_select "form[action=?][method=?]", pseudos_path, "post" do

      assert_select "input[name=?]", "pseudo[pseudo]"
    end
  end
end
