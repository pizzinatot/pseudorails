require 'rails_helper'

RSpec.describe "pseudos/show", type: :view do
  before(:each) do
    @pseudo = assign(:pseudo, Pseudo.create!(
      pseudo: "Pseudo"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Pseudo/)
  end
end
