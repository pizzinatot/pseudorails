require 'rails_helper'

RSpec.describe "pseudos/edit", type: :view do
  before(:each) do
    @pseudo = assign(:pseudo, Pseudo.create!(
      pseudo: "MyString"
    ))
  end

  it "renders the edit pseudo form" do
    render

    assert_select "form[action=?][method=?]", pseudo_path(@pseudo), "post" do

      assert_select "input[name=?]", "pseudo[pseudo]"
    end
  end
end
